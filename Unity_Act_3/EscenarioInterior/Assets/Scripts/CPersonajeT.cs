using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPersonajeT : MonoBehaviour
{
    public float speed = 10f;
    public float angularspeed = 30f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Rotation
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.up * (-angularspeed * Time.deltaTime));
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.up *  (angularspeed * Time.deltaTime));
        }

        //Movement
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * (Time.deltaTime * speed));
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back  * (Time.deltaTime * speed));
        }
    }
}
