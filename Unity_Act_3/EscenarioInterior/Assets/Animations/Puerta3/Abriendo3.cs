using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo3 : MonoBehaviour
{
    public Animator laPuerta3;

    private void OnTriggerEnter(Collider other)
    {
        laPuerta3.Play("Abrir3");
    }

    private void OnTriggerExit(Collider other)
    {
        laPuerta3.Play("Cerrar3");
    }
}
