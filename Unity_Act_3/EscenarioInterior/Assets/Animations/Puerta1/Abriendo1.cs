using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo1 : MonoBehaviour
{

    public Animator laPuerta1;

    private void OnTriggerEnter(Collider other)
    {
        laPuerta1.Play("Abrir1");
    }

    private void OnTriggerExit(Collider other)
    {
        laPuerta1.Play("Cerrar1");
    }
}
