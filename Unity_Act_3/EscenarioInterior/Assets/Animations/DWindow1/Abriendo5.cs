using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo5 : MonoBehaviour
{
    public Animator DWindow1;

    private void OnTriggerEnter(Collider other)
    {
        DWindow1.Play("Abrir5");
    }

    private void OnTriggerExit(Collider other)
    {
        DWindow1.Play("Cerrar5");
    }
}
