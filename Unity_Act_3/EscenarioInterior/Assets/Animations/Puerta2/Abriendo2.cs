using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo2 : MonoBehaviour
{
    public Animator laPuerta2;

    private void OnTriggerEnter(Collider other)
    {
        laPuerta2.Play("Abrir2");
    }

    private void OnTriggerExit(Collider other)
    {
        laPuerta2.Play("Cerrar2");
    }
}
