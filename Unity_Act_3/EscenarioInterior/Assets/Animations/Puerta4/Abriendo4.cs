using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo4 : MonoBehaviour
{
    public Animator laPuerta4;

    private void OnTriggerEnter(Collider other)
    {
        laPuerta4.Play("Abrir4");
    }

    private void OnTriggerExit(Collider other)
    {
        laPuerta4.Play("Cerrar4");
    }
}
