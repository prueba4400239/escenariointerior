using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo8 : MonoBehaviour
{
    public Animator DWindow4;

    private void OnTriggerEnter(Collider other)
    {
        DWindow4.Play("Abrir8");
    }

    private void OnTriggerExit(Collider other)
    {
        DWindow4.Play("Cerrar8");
    }
}
