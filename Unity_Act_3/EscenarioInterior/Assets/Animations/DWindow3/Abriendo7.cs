using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo7 : MonoBehaviour
{
    public Animator DWindow3;

    private void OnTriggerEnter(Collider other)
    {
        DWindow3.Play("Abrir7");
    }

    private void OnTriggerExit(Collider other)
    {
        DWindow3.Play("Cerrar7");
    }
}
