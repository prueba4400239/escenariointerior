using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abriendo6 : MonoBehaviour
{
    public Animator DWindow2;

    private void OnTriggerEnter(Collider other)
    {
        DWindow2.Play("Abrir6");
    }

    private void OnTriggerExit(Collider other)
    {
        DWindow2.Play("Cerrar6");
    }
}
